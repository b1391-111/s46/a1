import React, { useState } from 'react';
import PropTypes from 'prop-types';

// react-bootstrap component
import { Button, Card } from 'react-bootstrap';

const CourseCard = ({title, desc, price}) => {
    const [count, setCount] = useState(0);
    const [seats, setSeats] = useState(30);

    const thousands_separators = (num) => {
        let num_parts = num.toString().split(".");
        num_parts[0] = num_parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        return num_parts.join(".");
    }

    const enroll = () => {
        if(count < 30) {
            setCount(count + 1)
            setSeats(seats - 1)
        } else {
            alert("No More seats");
        }
    };

    return (
        <Card
            className="text-left"
        >
            <Card.Body>
                <Card.Title>{title}</Card.Title>
                <Card.Subtitle className="mb-2 text-dark">
                    Description: 
                    <p> {desc} </p>
                </Card.Subtitle>
                <Card.Text>
                    Price:
                    <Card.Text>PHP {thousands_separators(price)}</Card.Text>
                </Card.Text>
                <Card.Text>
                    Enrollees: {count}
                </Card.Text>
                <Button
                    onClick={enroll}
                >
                    Enroll
                </Button>
            </Card.Body>
        </Card>
    )
}

// Check if the CourseCard component is getting the correct prop data types
// PropTypes are used for validating information passed to a component and is a tool normally used to help developers ensure the correct information is passed from one component to the next.
CourseCard.propTypes = {
    // shape method => is used to check if a prop obj conforms to a specific "shape"
    title: PropTypes.string.isRequired,
    desc: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
}

export default CourseCard;
