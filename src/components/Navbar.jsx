import React from 'react';
import {
    Nav,
    Navbar,
} from 'react-bootstrap';

const Header = () => {
    return (
        <Navbar 
            bg="info" 
            expand="lg"
        >
            <Navbar.Brand href="/">
                React Booking
            </Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="me-auto">
                    <Nav.Link color="dark" href="#home">Home</Nav.Link>
                    <Nav.Link href="#courses">Courses</Nav.Link>
                </Nav>
            </Navbar.Collapse>
        </Navbar>
    )
}

export default Header;
